// 60mm flight simulator steam gauge
// Copyright September 2019 A M Errington
// Copyright January 2020 A M Errington
// All rights reserved
// You may copy, use, and redistribute this
// file under the terms of the CC BY-SA licence


// To build a basic gauge you need these five
// 3D-printed parts:
// Bezel, should be matte black, fine resolution
// Spacer, should be black
// Stepper plate to suit your stepper, can be any colour
// Indicator needle, should be black with a white painted tip
// Template, can be any colour


// In addition you also need the following:
// 1x 60x60x3mm white acrylic
// 1x 60x60x2mm clear acrylic
// 2x M3x14 countersunk head screw
// 4x M3 nut
// 2x M3x6 button/panel head screw
// 4x panel mounting screw, e.g. M4x16 hex/button head screw
// 3mm white LED
// LED resistor (optional)
// 4-pos connector
// 2-pos connector
// 6 pieces of thin wire
// decal
// Needle cover 8mm vinyl disc or sticker (optional)


// For a stepper with rear contacts, e.g. X27.168
// 4x M3x12 M-F spacer
// X27.168 stepper motor


// For a stepper with front contacts, e.g. X27.589 or x40.879
// X27.589 stepper motor
// 4x M4 nut

// Additionally for X40.879 dual stepper
// 4-pos connector
// Dual-needle assembly


width = 60;
height = 60;
corner_radius = 2;

// Set the stepper contacts here, to draw the
// correct previews and render the correct
// objects for printing.
stepper_contacts = "front";
//stepper_contacts = "rear";


module bezel(){
  difference(){
    union(){
      //Plate 60x60 r2 corners
      hull(){
        translate([(width/2)-corner_radius, (width/2)-corner_radius, 0])
        cylinder(r=2, h=2, $fn=20);
        translate([-((width/2)-corner_radius), (width/2)-corner_radius, 0])
        cylinder(r=2, h=2, $fn=20);
        translate([-((width/2)-corner_radius), -((width/2)-corner_radius), 0])
        cylinder(r=2, h=2, $fn=20);
        translate([(width/2)-corner_radius, -((width/2)-corner_radius), 0])
        cylinder(r=2, h=2, $fn=20);
      }
      // Bezel lower part
      translate([0, 0, 2])
      cylinder(r=(56/2), h=3.05, $fn=120);
      // Bezel upper (front) part
      translate([0, 0, 5])
      cylinder(r1=(56/2), r2=(54/2), h=3, $fn=120);
    }
    
    // Main opening
    translate([0,0,-0.5])
    cylinder(r=(47/2), h=12, $fn=90);
      
    // Chamfer inside of bezel
    translate([0, 0, 2])
    cylinder(r2=(50.5/2), r1=(47/2), h=6, $fn=120);
    
    translate([0,0,7.99])
    cylinder(r=(50.5/2), h=1, $fn=120);

    // Panel mounting holes 47x47 M4 clear
    translate([0, 0, -0.5]){
      translate([23.5, 23.5, 0])
      cylinder(r=2.25, h=3, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=2.25, h=3, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=2.25, h=3, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=2.25, h=3, $fn=30);
    }

    // Alignment mark
    translate([15, -30, -0.5])
    cylinder(r=0.5, h=5, $fn=20);

    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.5]){
      translate([26.25, 17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([26.25, -17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([-26.25, -17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([-26.25, 17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
    }

    // Assembly holes M3 countersink
    translate([26.25, -17.15, 0.14])
    cylinder(r1 = 1.5, r2 = 3.36, h = 1.86, $fn = 30);
    translate([-26.25, 17.15, 0.14])
    cylinder(r1 = 1.5, r2 = 3.36, h = 1.86, $fn = 30);

    translate([26.25, -17.15, 1.99])
    cylinder(r=3.36, h=1, $fn=20);
    translate([-26.25, 17.15, 1.99])
    cylinder(r=3.36, h=1, $fn=20);

  } // difference()

} // module


module stepper_plate_centre_rear(){
  // Stepper needle is centred at (0,0)
  // Stepper is X27-168 with rear contacts
  // mounted on the front of this plate.
  difference(){
    //Plate 60x40.0 r2 corners
    union(){
      hull(){
        translate([28, 18.9, 0])
        cylinder(r=2, h=2, $fn=20);
        translate([28, -28, 0])
        cylinder(r=2, h=2, $fn=20);
        translate([-28, -28, 0])
        cylinder(r=2, h=2, $fn=20);
        translate([-28, 18.9, 0])
        cylinder(r=2, h=2, $fn=20);
      }
      cylinder(r=30, h=2, $fn=60);
    }
    
    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.5]){
      translate([26.25, 17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([26.25, -17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([-26.26, -17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
      translate([-26.25, 17.15, 0])
      cylinder(r=1.6, h=3, $fn=20);
    }
    
    // Alignment mark
    translate([15, -30, -0.5])
    cylinder(r=0.5, h=3, $fn=20);
    
    // Large peg dia. 3.9. PCB hole dia. 3.8
    translate([0, 4, -0.5])
    cylinder(r=2, h=3, $fn=20);
    // Small peg dia. 2.6. PCB hole dia. 2.5
    translate([-9, -15, -0.5])
    cylinder(r=1.3, h=3, $fn=20);
    // Alignment holes (optionally use filament as a pin)
    translate([13.5, -6, -0.5])
    cylinder(r=1, h=3, $fn=20);
    translate([-13.5, -6, -0.5])
    cylinder(r=1, h=3, $fn=20);
    // Stepper wire holes
    translate([11.43, -6+(7.62/2), -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    translate([-11.43, -6+(7.62/2), -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    translate([-11.43, -6-(7.62/2), -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    translate([11.43, -6-(7.62/2), -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    
    // LED wire holes
    translate([-1.27, 27, -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    translate([1.27, 27, -0.5])
    cylinder(r=0.6, h=3, $fn=20);
    
    // Rebate for connectors (on back)
    // and connector pin holes
    translate([-2.54-0.25, -23.5-2.88-0.25, -0.5])
    cube([5.08+0.5, 5.8+0.5, 1.5]);
    translate([-1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);

    translate([-15.24-0.25, -23.5-2.88-0.25, -0.5])
    cube([10.16+0.5, 5.8+0.5, 1.5]);
    translate([-13.97, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-11.43, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-8.89, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-6.35, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);

    // Remove some material to reduce
    // printing time and amount of
    // filament
    for (i=[60: -30: -210]){
      rotate([0, 0, -i])
      translate([21.5, 0, -0.5])
      cylinder(r=4, h=3, $fn=20);
    }
    
    for (i=[-30, -90, -150]){
      rotate([0, 0, -i])
      translate([11.5, 0, -0.5])
      cylinder(r=4, h=3, $fn=20);
    }
        
    translate([0, -6, -0.5])
    cylinder(r=4, h=3, $fn=20);
    
    // Mounting holes clearance
    translate([23.5, -23.5, -0.5])
    cylinder(r=4, h=3, $fn=20);
    translate([-23.5, -23.5, -0.5])
    cylinder(r=4, h=3, $fn=20);
    translate([-23.5, 23.5, -0.5])
    cylinder(r=4, h=3, $fn=20);
    translate([23.5, 23.5, -0.5])
    cylinder(r=4, h=3, $fn=20);
    
  } // difference

} // module


module stepper_plate_centre_front(){
  // Stepper needle is centred at (0,0)
  // Stepper is X27-589 (single shaft) or
  // X40-879 (dual concentric shaft) with
  // front contacts mounted on the rear of
  // this plate.

  difference(){
    //Plate 60x60.0 r6.5 corners 4.4mm thick
    hull(){
      translate([23.5, 23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
    }

    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.1]){
      translate([26.25, 17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([26.25, -17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([-26.26, -17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([-26.25, 17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
    }

    // Nut traps 52.5x34.3 M3 (6.35 x 2.4)
    translate([0, 0, -0.1]){
      translate([26.25, 17.15, 0])
      rotate([0,0,27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([26.25, -17.15, 0])
      rotate([0,0,-27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([-26.26, -17.15, 0])
      rotate([0,0,27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([-26.25, 17.15, 0])
      rotate([0,0,-27])
      cylinder(d=6.6, h=2.8, $fn=6);
    }

    // Nut trap holes 47x47
    translate([0, 0, -0.1]){
      translate([23.5, 23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([23.5, -23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([-23.5, -23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([-23.5, 23.5, 0])
      cylinder(r=3, h=5, $fn=20);
    }

    // Nut traps 47x47 M4 (8.08 x 3.2)
    translate([0, 0, 0.9]){
      translate([23.5, 23.5, 0])
      rotate([0,0,27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([23.5, -23.5, 0])
      rotate([0,0,-27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([-23.5, -23.5, 0])
      rotate([0,0,27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([-23.5, 23.5, 0])
      rotate([0,0,-27])
      cylinder(r=4.1, h=5, $fn=6);
    }

    // Alignment mark
    translate([15, -30, -0.5])
    cylinder(r=0.5, h=5, $fn=20);

    // Marker I for single needle stepper
    translate([-18.735+0.5,-27.5,3.6])
    cube([1,4,1]);

    // Marker II for dual-needle stepper
    translate([18.735-0.5,-27.5,3.6])
    cube([1,4,1]);
    translate([18.735-2.5,-27.5,3.6])
    cube([1,4,1]);

    // Stepper pegs dia. 2.5. PCB hole dia. 3
    translate([8.49, 2.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-8.49, 2.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([0, -16.75, -0.5])
    cylinder(r=1.5, h=5, $fn=20);

    // Stepper alignment holes
    // (optionally use filament as a pin)
    translate([13.5, -6, -0.5])
    cylinder(r=1, h=3, $fn=20);
    translate([-13.5, -6, -0.5])
    cylinder(r=1, h=3, $fn=20);

    // Stepper wire holes (inner)
    translate([11.43, -6+(7.62/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-11.43, -6+(7.62/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-11.43, -6-(7.62/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([11.43, -6-(7.62/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);

    translate([11.43, -6+(7.62/2), 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-11.43, -6+(7.62/2), 1])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=5, $fn=4);
    translate([-11.43, -6-(7.62/2), 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([11.43, -6-(7.62/2), 1])
    cylinder(r=1.5, h=5, $fn=20);

    // Stepper wire holes (outer)
    // For dual-needle stepper only.
    translate([13.97, -6+(12.72/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-13.97, -6+(12.72/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-13.97, -6-(12.72/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([13.97, -6-(12.72/2), -0.1])
    cylinder(r=0.6, h=5, $fn=20);

    translate([13.97, -6+(12.72/2), 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-13.97, -6+(12.72/2), 1])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=5, $fn=4);
    translate([-13.97, -6-(12.72/2), 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([13.97, -6-(12.72/2), 1])
    cylinder(r=1.5, h=5, $fn=20);

    // LED wire holes
    translate([-1.27, 27, -0.5])
    cylinder(r=0.6, h=5, $fn=20);
    translate([1.27, 27, -0.5])
    cylinder(r=0.6, h=5, $fn=20);

    // LED flange
    translate([0, 27, 2])
    cylinder(r=2.1, h=2.5, $fn=20);

    // Rebate for connectors (on back)
    // and connector pin holes
    // 2P Body
    translate([-2.54-0.25, -23.5-2.88-0.25, -0.1])
    cube([5.08+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([-1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-1.27, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([1.27, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    // 4P Body
    translate([-15.24-0.25, -23.5-2.88-0.25, -0.1])
    cube([10.16+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([-13.97, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-13.97, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-11.43, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-11.43, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-8.89, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-8.89, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-6.35, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-6.35, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    // 4P Body
    translate([5.08-0.25, -23.5-2.88-0.25, -0.1])
    cube([10.16+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([13.97, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([13.97, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    translate([11.43, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([11.43, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([8.89, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([8.89, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([6.35, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([6.35, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    // Remove some material to reduce
    // printing time and amount of filament
    for (i=[30: -30: -210]){
      rotate([0, 0, -i])
      translate([21.5, 0, -0.5])
      cylinder(r=4, h=5, $fn=20);
    }

    for (i=[-60, -120, 60, 120]){
      rotate([0, 0, -i])
      translate([11.5, 0, -0.5])
      cylinder(r=4, h=5, $fn=20);
    }

    // Centre hole for needle
    translate([0, 0, -0.5])
    cylinder(r=4, h=5, $fn=20);

    // Remove 1/2 thickness, but leave supporting
    // boss in centre, and support pegs near
    // connectors
    translate([0, 0, 2])
    difference(){
      // Large circle removes most material
      cylinder(r=28, h=2.5, $fn=60);
      // Leave material for centre boss
      cylinder(r=6, h=2.5, $fn=30);
      // Leave material for support pins
      translate([-10.16,-16.75,-0.1])
      cylinder(r=2, h=5, $fn=20);
      translate([10.16,-16.75,-0.1])
      cylinder(r=2, h=5, $fn=20);
    }

    // Make a central hole in support pins to
    // strengthen printing  
    translate([-10.16,-16.75,-0.1])
    cylinder(r=0.5, h=5, $fn=20);
    translate([10.16,-16.75,-0.1])
    cylinder(r=0.5, h=5, $fn=20);

  } // difference

} // module


module stepper_plate_pair_front(){
  // Stepper needles are centred at (-20.25,0)
  // and (20.25,0)
  // Stepper is X27-589 (single shaft) with
  // front contacts mounted on the rear of
  // this plate.

  difference(){
    //Plate 60x60.0 r6.5 corners 4.4mm thick
    hull(){
      translate([23.5, 23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=6.5, h=4.4, $fn=30);
    }

    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.1]){
      translate([26.25, 17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([26.25, -17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([-26.26, -17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
      translate([-26.25, 17.15, 0])
      cylinder(r=1.7, h=5, $fn=20);
    }

    // Nut traps 52.5x34.3 M3 (6.35 x 2.4)
    translate([0, 0, -0.1]){
      translate([26.25, 17.15, 0])
      rotate([0,0,27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([26.25, -17.15, 0])
      rotate([0,0,-27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([-26.26, -17.15, 0])
      rotate([0,0,27])
      cylinder(d=6.6, h=2.8, $fn=6);
      translate([-26.25, 17.15, 0])
      rotate([0,0,-27])
      cylinder(d=6.6, h=2.8, $fn=6);
    }

    // Nut trap holes 47x47
    translate([0, 0, -0.1]){
      translate([23.5, 23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([23.5, -23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([-23.5, -23.5, 0])
      cylinder(r=3, h=5, $fn=20);
      translate([-23.5, 23.5, 0])
      cylinder(r=3, h=5, $fn=20);
    }

    // Nut traps 47x47 M4 (8.08 x 3.2)
    translate([0, 0, 0.9]){
      translate([23.5, 23.5, 0])
      rotate([0,0,27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([23.5, -23.5, 0])
      rotate([0,0,-27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([-23.5, -23.5, 0])
      rotate([0,0,27])
      cylinder(r=4.1, h=5, $fn=6);
      translate([-23.5, 23.5, 0])
      rotate([0,0,-27])
      cylinder(r=4.1, h=5, $fn=6);
    }

    // Alignment mark
    translate([15, -30, -0.5])
    cylinder(r=0.5, h=5, $fn=20);

    // Stepper pegs dia. 2.5. PCB hole dia. 3
    // Left
    translate([-22.74, 8.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-22.74, -8.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-3.5, 0, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    // Right
    translate([22.74, 8.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([22.74, -8.49, -0.5])
    cylinder(r=1.5, h=5, $fn=20);
    translate([3.5, 0, -0.5])
    cylinder(r=1.5, h=5, $fn=20);

    // Stepper alignment holes
    // (optionally use filament as a pin)
    // Left
    translate([-14.25, 13.5, -0.5])
    cylinder(r=1, h=3, $fn=20);
    translate([-14.25, -13.5, -0.5])
    cylinder(r=1, h=3, $fn=20);
    // Right
    translate([14.25, 13.5, -0.5])
    cylinder(r=1, h=3, $fn=20);
    translate([14.25, -13.5, -0.5])
    cylinder(r=1, h=3, $fn=20);

    // Stepper wire holes
    // Left
    translate([-14.25-(7.62/2), 11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-14.25+(7.62/2), 11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-14.25-(7.62/2), -11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([-14.25+(7.62/2), -11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);

    translate([-14.25-(7.62/2), 11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-14.25+(7.62/2), 11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([-14.25-(7.62/2), -11.43, 1])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=5, $fn=4);
    translate([-14.25+(7.62/2), -11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);

    // Right
    translate([14.25-(7.62/2), 11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([14.25+(7.62/2), 11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([14.25-(7.62/2), -11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);
    translate([14.25+(7.62/2), -11.43, -0.1])
    cylinder(r=0.6, h=5, $fn=20);

    translate([14.25-(7.62/2), 11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([14.25+(7.62/2), 11.43, 1])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=5, $fn=4);
    translate([14.25-(7.62/2), -11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);
    translate([14.25+(7.62/2), -11.43, 1])
    cylinder(r=1.5, h=5, $fn=20);

    // LED wire holes
    translate([-1.27, 27, -0.5])
    cylinder(r=0.6, h=5, $fn=20);
    translate([1.27, 27, -0.5])
    cylinder(r=0.6, h=5, $fn=20);

    // LED flange
    translate([0, 27, 2])
    cylinder(r=2.1, h=2.5, $fn=20);

    // Rebate for connectors (on back)
    // and connector pin holes
    // 2P Body
    translate([-2.54-0.25, -23.5-2.88-0.25, -0.1])
    cube([5.08+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([-1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-1.27, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([1.27, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([1.27, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    // 4P Body
    translate([-15.24-0.25, -23.5-2.88-0.25, -0.1])
    cube([10.16+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([-13.97, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-13.97, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-11.43, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-11.43, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-8.89, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-8.89, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([-6.35, -23.5, -0.1])
    cylinder(r=0.6, h=3, $fn=30);
    translate([-6.35, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    // 4P Body
    translate([5.08-0.25, -23.5-2.88-0.25, -0.1])
    cube([10.16+0.5, 5.8+0.5, 0.4]);
    // Pins
    translate([13.97, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([13.97, -23.5, 1.5])
    rotate([0,0,45])
    cylinder(r=1.5*1.414, h=3, $fn=4);

    translate([11.43, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([11.43, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([8.89, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([8.89, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    translate([6.35, -23.5, -0.5])
    cylinder(r=0.6, h=3, $fn=30);
    translate([6.35, -23.5, 1.5])
    cylinder(r=1.5, h=3, $fn=30);

    // Remove some material to reduce
    // printing time and amount of filament
    for (i=[-60, -90, -120]){
      rotate([0, 0, -i])
      translate([21.5, 0, -0.5])
      cylinder(r=4, h=5, $fn=20);
    }

    for (i=[-60, -120, 0, 180, 60, 120]){
      rotate([0, 0, -i])
      translate([11.5, 0, -0.5])
      cylinder(r=4, h=5, $fn=20);
    }

    // Centre hole for needles
    // Left
    translate([-20.25, 0, -0.5])
    cylinder(r=4, h=5, $fn=20);
    // Right
    translate([20.25, 0, -0.5])
    cylinder(r=4, h=5, $fn=20);

    // Remove 1/2 thickness, but leave supporting
    // boss in centre, and support pegs near
    // connectors
    translate([0, 0, 2])
    difference(){
      // Large circle removes most material
      cylinder(r=28, h=2.5, $fn=60);
      // Leave material for centre boss
      // Left
      translate([-20.25,0,0])
      cylinder(r=6, h=2.5, $fn=30);
      // Right
      translate([20.25,0,0])
      cylinder(r=6, h=2.5, $fn=30);
      // Leave material for support pins
      translate([-10.16,-16.75,-0.1])
      cylinder(r=2, h=5, $fn=20);
      translate([10.16,-16.75,-0.1])
      cylinder(r=2, h=5, $fn=20);
      translate([0,6,-0.1])
      cylinder(r=2, h=5, $fn=20);
    }

    // Make a central hole in support pins to
    // strengthen printing
    translate([-10.16,-16.75,-0.1])
    cylinder(r=0.5, h=5, $fn=20);
    translate([10.16,-16.75,-0.1])
    cylinder(r=0.5, h=5, $fn=20);
    translate([0,6,-0.1])
    cylinder(r=0.5, h=5, $fn=20);

  } // difference

} // module


module template(){
  // Template for cutting acrylic pieces.
  //
  // All drill holes are 3mm, to be opened
  // up as required.
  // To use the template, cut the 3mm white
  // acrylic for the back and 2mm transparent
  // acrylic for the front to 60x60mm (or use
  // the outer edge of the template). Mark the
  // alignment notch on the edge of the acrylic.
  // Drill the 4 pairs of outer assembly holes
  // with a 3mm drill. This is sufficient for
  // the M3 clear holes. Other holes can be
  // opened up later.
  //
  // For the front 2mm clear acrylic:
  // The 50mm diameter circle must be scribed in
  // the *underside* of the acrylic. Use the
  // alignment notch to indicate which side is
  // being worked on.
  // A sharp point can be used to scribe three arcs
  // inside the template, then the template can be
  // rotated 90- or 180-degrees to join up the
  // ends of the arcs.
  // Optionally a small indent can be drilled into
  // the underside of the clear acrylic using the
  // top centre guide hole in the template. This
  // indent may help with dispersing the LED light.
  // Finally, open up the outermost four holes to
  // 4mm (M4 clear)
  //
  // For the rear 3mm white acrylic:
  // Use the template and 3mm drill to drill the
  // top centre hole hole for the LED, and the
  // centre hole for the needle.
  // Open up the LED hole to 4mm diameter to
  // accept the LED flange.
  // Open up the centre hole to 6.5mm diameter
  // or as required to accept the gauge needle boss.
  // For the front-contact stepper (X27.589 and
  // X40.879) the outermost four holes should be
  // opened up to 4mm (M4 clear).
  // For the rear-contact stepper (X27.168) the
  // outermost four holes should be threaded to 
  // suit the panel mounting screws, e.g. M4
  //

  difference(){
    //Plate 60x60 3mm thick
    translate([-30, -30, 0])
    cube([60,60,3]);
    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.1]){
      translate([26.25, 17.15, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([26.25, -17.15, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([-26.26, -17.15, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([-26.25, 17.15, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
    }

    // Assembly holes 47x47 M4 clear or threaded
    translate([0, 0, -0.1]){
      translate([23.5, 23.5, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([23.5, -23.5, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([-23.5, -23.5, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
      translate([-23.5, 23.5, 0])
      cylinder(r=1.6, h=3.2, $fn=20);
    }

    // Alignment mark
    translate([15, -30, -0.5])
    cylinder(r=0.5, h=4, $fn=20);

    // LED hole 4mm diameter for flange
    translate([0, 27, -0.1])
    cylinder(r=1.6, h=3.2, $fn=20);

    // Remove central material to make
    // scribing guide for transparent acrylic
    // Keep three support arms for centre boss
    translate([0, 0, -0.5])
    difference(){
      // Radius for scribing is 0.5mm less
      // than opening in spacer
      cylinder(r=(51/2)-0.5, h=4, $fn=60); 
      // Centre boss
      cylinder(r=4, h=4, $fn=60);
      // Guide for twin needles
      translate([-20.25,0,0])
      cylinder(r=4, h=4, $fn=60);
      translate([20.25,0,0])
      cylinder(r=4, h=4, $fn=60);
      // Radial arms
      translate([-3,0,0])
      cube([6,30,4]);
      rotate([0,0,120])
      translate([-3,0,0])
      cube([6,30,4]);
      rotate([0,0,-120])
      translate([-3,0,0])
      cube([6,30,4]);
      // Twin needle support
      translate([-30,-3,0])
      cube([10,6,4]);
      translate([20,-3,0])
      cube([10,6,4]);
    }

    // Centre hole
    translate([0, 0, -0.5])
    cylinder(r=1.6, h=4, $fn=20);
    // Guide for twin needles
    translate([-20.25,0,-0.5])
    cylinder(r=1.6, h=4, $fn=60);
    translate([20.25,0,-0.5])
    cylinder(r=1.6, h=4, $fn=60);

  } // difference

} // module template()


module spacer(){
  difference(){
    //Plate 60x60 r2 corners
    hull(){
      translate([28,28,0])
      cylinder(r=2,h=4,$fn=20);
      translate([28,-28,0])
      cylinder(r=2,h=4,$fn=20);
      translate([-28,-28,0])
      cylinder(r=2,h=4,$fn=20);
      translate([-28,28,0])
      cylinder(r=2,h=4,$fn=20);
    }

    // Main opening
    translate([0,0,-0.5])
    difference(){
      cylinder(r=(51/2),h=5,$fn=90);
      translate([0,50,0])
      cylinder(r=(51/2),h=5,$fn=90);
    }

    // Panel mounting holes 47x47 M4 clear
    translate([0,0,-0.5])
    {
    translate([23.5,23.5,0])
    cylinder(r=2.25,h=5,$fn=20);
    translate([23.5,-23.5,0])
    cylinder(r=2.25,h=5,$fn=20);
    translate([-23.5,-23.5,0])
    cylinder(r=2.25,h=5,$fn=20);
    translate([-23.5,23.5,0])
    cylinder(r=2.24,h=5,$fn=20);
    }
    // Assembly holes 52.5x34.3 M3 clear
    translate([0, 0, -0.5]){
    translate([26.25, 17.15, 0])
    cylinder(r=1.6, h=5, $fn=20);
    translate([26.25, -17.15, 0])
    cylinder(r=1.6, h=5, $fn=20);
    translate([-26.25, -17.15, 0])
    cylinder(r=1.6, h=5, $fn=20);
    translate([-26.25, 17.15, 0])
    cylinder(r=1.6, h=5, $fn=20);
    }

    // Assembly holes rebate 2mm on rear face
    translate([26.25,17.15,-0.5])
    cylinder(r=3.1,h=3,$fn=30);
    translate([-26.25,-17.15,-0.5])
    cylinder(r=3.1,h=3,$fn=30);  

    // Alignment mark
    translate([15,-30,0])
    cylinder(r=0.5,h=4,$fn=20);

    // LED hole 3mm
    translate([0,27,-0.5])
    cylinder(r=1.6, h=5, $fn=20);

    // LED flange 4mm
    translate([0,27,-0.5])
    cylinder(r=2.1,h=1.5,$fn=20);

  } // difference

} // module


module needle_sgl_18mm(){
  // 60mm gauge 18mm needle

  difference(){
    union(){
      // Centre boss
      // The spacer makes a gap of 4mm. The
      // stepper needle protrudes 3.5mm into
      // this gap.
      color("black")
      cylinder(r=1.6, h=0.5, $fn=30);
      translate([0,0,0.5])
      color("black")
      cylinder(r=4, h=3.0, $fn=40);
      // Pointer
      translate([0,0,1.5])
      color("white")
      linear_extrude(height=1,center=false)
      polygon(points=[[-1,0],[-1,16],[0,18],[1,16],[1,0]]);

    }
    // Stepper needle is dia. 1mm
    // Use r=0.6 for this printer
    translate([0,0,-0.5])
    cylinder(r=0.6, h=7.22, $fn=20);
  }

} // module

if ($preview){
  // Build a model of the complete assembly
  if (stepper_contacts == "rear")
  { // preview rear contact motor
  // Back
  translate([0,0,0.1]) // Prevent rendering artefacts
  color("white") 
  difference(){
    // 3mm acrylic plate
    translate([-30,-30,0])
    cube([60,60,3]);
    
    translate([0,0,-0.5]){
      // M3 clear holes
      translate([26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([26.25,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.26,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      
      // M4 tapped holes
      translate([23.5, 23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);

      // Needle hole 6.35mm (1/4") clear
      cylinder(r=3.2,h=4,$fn=30);
      
      // LED hole
      translate([0,27,0])
      cylinder(r=2,h=4,$fn=30);
    }
  }

  translate([0,0,3.02]) // Prevent artefacts
  color("gray") spacer();

  translate([0,0,3.01]) // Prevent artefacts
  color("black",0.9) cylinder(d=52,h=0.1);

  translate([0, 0, 3.02-3.22]) // Prevent artefacts
  needle_sgl_18mm();

  // Glass
  translate([0,0,7.03]) // Prevent artefacts
  color("azure",0.25)
  difference(){
    // 2mm clear acrylic plate
    translate([-30,-30,0])
    cube([60,60,2]);

    translate([0,0,-0.5]){
      // M3 clear holes
      translate([26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([26.25,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.26,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
    
      // M4 clear holes
      translate([23.5, 23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=2, h=4, $fn=30);
    }
  }

  translate([0,0,9.04]) // Prevent artefacts
  color("gray") bezel();

  translate([0,0,-14.01])
  color("white") stepper_plate_centre_rear();

  // Hex standoffs
  color("gold")
  translate([26.25,17.15,-12])
  difference(){
    cylinder(r=3,h=12,$fn=6);
    translate([0,0,-0.5])
    cylinder(r=1.5,h=13,$fn=20);
  }
  translate([26.25,17.15,-18])
  cylinder(r=1.5,h=9,$fn=20);

  color("gold")
  translate([26.25,-17.15,-12])
  difference(){
  cylinder(r=3,h=12,$fn=6);
  translate([0,0,-0.5])
  cylinder(r=1.5,h=13,$fn=20);
  }
  translate([26.25,-17.15,-18])
  cylinder(r=1.5,h=9,$fn=20);

  color("gold")
  translate([-26.25,-17.15,-12])
  difference(){
  cylinder(r=3,h=12,$fn=6);
  translate([0,0,-0.5])
  cylinder(r=1.5,h=13,$fn=20);
  }
  translate([-26.25,-17.15,-18])
  cylinder(r=1.5,h=9,$fn=20);

  color("gold")
  translate([-26.25,17.15,-12])
  difference(){
  cylinder(r=3,h=12,$fn=6);
  translate([0,0,-0.5])
  cylinder(r=1.5,h=13,$fn=20);
  }
  translate([-26.25,17.15,-18])
  cylinder(r=1.5,h=9,$fn=20);

} // preview rear contact motor
else
{ // preview front contact motor
  // Back
  translate([0,0,0.1]) // Prevent rendering artefacts
  color("white") 
  difference(){
    // 3mm acrylic plate
    translate([-30,-30,0])
    cube([60,60,3]);
    
    translate([0,0,-0.5]){
      // M3 clear holes
      translate([26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([26.25,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.26,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      
      // M4 tapped holes
      translate([23.5, 23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=1.7, h=4, $fn=30);

      // Needle hole 6.35mm (1/4") clear
      cylinder(r=3.2,h=4,$fn=30);
      
      // LED hole
      translate([0,27,0])
      cylinder(r=2,h=4,$fn=30);
    }
  }

  translate([0,0,3.02]) // Prevent artefacts
  color("gray") spacer();

  translate([0,0,3.01]) // Prevent artefacts
  color("black",0.9) cylinder(d=52,h=0.1);

  
  translate([0, 0, 3.02-3.22]) // Prevent artefacts
  needle_sgl_18mm();
  

  // Glass
  translate([0,0,7.03]) // Prevent artefacts
  color("azure",0.25)
  difference(){
    // 2mm clear acrylic plate
    translate([-30,-30,0])
    cube([60,60,2]);
    
    translate([0,0,-0.5]){
      // M3 clear holes
      translate([26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([26.25,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.26,-17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
      translate([-26.25,17.15,0])
      cylinder(r=1.5,h=4,$fn=20);
    
      // M4 clear holes
      translate([23.5, 23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([23.5, -23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([-23.5, -23.5, 0])
      cylinder(r=2, h=4, $fn=30);
      translate([-23.5, 23.5, 0])
      cylinder(r=2, h=4, $fn=30);
    }
  }

  translate([0,0,9.04]) // Prevent artefacts
  color("gray") bezel();

  translate([0,0,-4.01])
  color("white") stepper_plate_centre_front();


} // preview front contact motor
}
else
{
  // Render individual components for printing

  // SPACER
  // Flip the spacer over so that the rebates
  // for the screw heads are not overhangs.
  translate([40,-40,4])
  rotate([180,0,0])
  spacer();

  // STEPPER PLATE

  if (stepper_contacts == "rear") {
    translate([-40,-40,2])
    // Flip the stepper plate over so that
    // the connector rebates are not overhangs.
    rotate([180,0,0])
    stepper_plate_centre_rear();
  }
  else
  {
    translate([-40,-40,0])
    stepper_plate_centre_front();
  }

  // BEZEL
  translate([-40,40,0])
  bezel();

  // DRILLING TEMPLATE  
  translate([40,40,0])
  template();
  
  // NEEDLE
  needle_sgl_18mm();

}
