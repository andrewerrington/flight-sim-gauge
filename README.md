# Flight sim gauge

3D model and other files to build flight simulator gauges.

Software is licenced under the GPL v3.0 unless otherwise stated.

3D models are licenced CC BY-SA unless otherwise stated.